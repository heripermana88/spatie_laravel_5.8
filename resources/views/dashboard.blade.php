@extends('layouts.joli.main')
@section('title',' Dashboard')

@section('content')
<div class="row">
    <div class="col-md-7">
        
        <!-- START USERS ACTIVITY BLOCK -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Users Activity</h3>
                    <span>Users vs returning</span>
                </div>                                    
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>                                        
                    </li>                                        
                </ul>                                    
            </div>                                
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-bar-1" style="height: 200px;"></div>
            </div>                                    
        </div>
        <!-- END USERS ACTIVITY BLOCK -->
        
    </div>
    <div class="col-md-5">
        
        <!-- START VISITORS BLOCK -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Visitors</h3>
                    <span>Visitors (last month)</span>
                </div>
                <ul class="panel-controls" style="margin-top: 2px;">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                        <ul class="dropdown-menu">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                        </ul>                                        
                    </li>                                        
                </ul>
            </div>
            <div class="panel-body padding-0">
                <div class="chart-holder" id="dashboard-donut-1" style="height: 200px;"></div>
            </div>
        </div>
        <!-- END VISITORS BLOCK -->
    </div>
</div>

<div class="row">
    <div class="col-md-3">                        
        <a href="#" class="tile tile-default">
            254
            <p>Domains</p>
            <div class="informer informer-success dir-tr"><span class="fa fa-caret-up"></span></div>
        </a>                        
    </div>
    <div class="col-md-3">                        
        <a href="#" class="tile tile-default">
            254
            <p>Yearly Growth</p>
            <div class="informer informer-success dir-tr"><span class="fa fa-caret-up"></span></div>
        </a>                        
    </div>
    <div class="col-md-3">                        
        <a href="#" class="tile tile-default">
            254
            <p>Monthly Growth</p>
            <div class="informer informer-success dir-tr"><span class="fa fa-caret-up"></span></div>
        </a>                        
    </div>
    <div class="col-md-3">                        
        <a href="#" class="tile tile-default">
            254
            <p>Daily Growth</p>
            <div class="informer informer-success dir-tr"><span class="fa fa-caret-up"></span></div>
        </a>                        
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- START DEFAULT DATATABLE -->
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Domain</th>
                            <th>Registration Date</th>
                            <th>Kontak</th>
                            <th>Tanggal Akhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for($i = 0; $i < 100; $i++)
                        <tr>
                            <td>domain{{$i+1}}.id</td>
                            <td>2020-{{rand(01,12)}}-{{rand(01,28)}}</td>
                            <td>Bachriey</td>
                            <td>202{{rand(1,3)}}-{{rand(01,12)}}-{{rand(01,28)}}</td>
                        </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/morris.min.js') }}"></script>       
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/d3.v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/rickshaw.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>                
<script type='text/javascript' src="{{ asset('joli/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
<script type="text/javascript" src="{{ asset('joli/js/plugins/owl/owl.carousel.min.js') }}"></script>                 

<script type="text/javascript" src="{{ asset('joli/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
@endsection