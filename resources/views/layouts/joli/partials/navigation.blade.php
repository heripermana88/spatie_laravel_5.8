<ul class="x-navigation">
    <li class="xn-logo">
        <a href="index.html">INA17.ID</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
        <a href="#" class="profile-mini">
            {{-- <img src="{{ asset('joli/assets/images/users/avatar.jpg') }}" alt="John Doe"/> --}}
            <h1 style="padding:0;margin:0"><strong>ID</strong></h1>
        </a>
        {{-- <div class="profile">
            <div class="profile-image">
                <img src="{{ asset('joli/assets/images/users/avatar.jpg') }}" alt="John Doe"/>
            </div>
            <div class="profile-data">
                <div class="profile-data-name">John Doe</div>
                <div class="profile-data-title">Web Developer/Designer</div>
            </div>
            <div class="profile-controls">
                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
            </div>
        </div>                                                                         --}}
    </li>
    <li class="xn-title">Navigation</li>
    <?php $menus = \App\Models\Menu::where('status','active')->get();?>
    @foreach($menus as $key => $m)
        @if($m['slug']=='link' && $m['parent_id']==null)
            <li><a href="{{ $m['href']  }}"><span class="fa {{$m['icon']}}"></span> <span class="xn-text">{{ $m['name'] }}</span></a></li>
        @elseif($m['slug']=='dropdown' && $m['parent_id']==null)
        {{-- @can(str_replace('&','_',str_replace(' ','',$m['name']))) --}}
        <?php $parent = $m['id'] ?>
        <li class="xn-openable {{ Request::segment(1)==$m['href'] ? 'active' :'' }}">
            <a href="#"><span class="fa {{$m['icon']}}"></span> <span class="xn-text">{{ $m['name'] }}</span></a>
            <ul>
                @foreach($menus as $key => $mm)
                    @if($parent==$mm['parent_id'])
                        @if($mm['slug']=='link')
                        <li><a href="{{url($mm['href'])}}"><span class="fa"></span> {{$mm['name']}}</a></li>
                        @else
                        <li class="xn-openable">
                            <a href="{{url($mm['href'])}}"><span class="fa"></span> {{$mm['name']}}</a>
                            <ul>
                                @foreach($menus as $key => $mmm)
                                @if($mm['id']==$mmm['parent_id'])
                                <li><a href="{{url($mmm['href'])}}"><span class="fa"></span> {{$mmm['name']}}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </li>
                        @endif
                    @endif
                @endforeach
            </ul>
        </li>

        {{-- @endcan --}}
        @endif
    @endforeach
</ul>