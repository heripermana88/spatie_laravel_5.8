@extends('layouts.joli.main')

@section('title',' Contact Detail')

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-body">
               
               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">General Information</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Contact Name/Role</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Organozation</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Trading Name</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                           </div>
                     </div>
               </div>
               
               <br>
               
               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">Address & Telephone</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Telephone</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Fax</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>State Province</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Postcode</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Country</label>
                                        
                                        <select name="" id="input" class="form-control" required="required">
                                            <option value=""></option>
                                        </select>
                                        
                                    </div>    
                                </div>
                           </div>
                     </div>
               </div>
               
               <br>
               
               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">Notification Email Address</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Email Primer</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-lg-12 col-md-12" style="padding:10px 0;">
                                    &nbsp;
                                    <hr>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Billing Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Transfer Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Security Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Launch Notification Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Legar Contract Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                           </div>
                     </div>
               </div>
               
               <br>

               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">Optional Notification Email Address</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>MArketing Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Oprational Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>Credit Limit Warning Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Deletion Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                           </div>
                     </div>
               </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

@endsection