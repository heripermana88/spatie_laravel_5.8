@extends('layouts.joli.main')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit New User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>
</div>


@if (count($errors) > 0)
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


{!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Password:</strong>
            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Confirm Password:</strong>
            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control'))
            !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Role:</strong>
            {!! Form::select('roles[]', $roles,$userRole, array('class' => 'form-control','multiple')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
{!! Form::close() !!}


@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/morris.min.js') }}"></script>       
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/d3.v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/rickshaw.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>                
<script type='text/javascript' src="{{ asset('joli/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
<script type="text/javascript" src="{{ asset('joli/js/plugins/owl/owl.carousel.min.js') }}"></script>                 

<script type="text/javascript" src="{{ asset('joli/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
@endsection