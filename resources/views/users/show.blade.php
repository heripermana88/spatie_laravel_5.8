@extends('layouts.joli.main')


@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show User</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {{ $user->name }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Email:</strong>
            {{ $user->email }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Roles:</strong>
            @if(!empty($user->getRoleNames()))
            @foreach($user->getRoleNames() as $v)
            <label class="badge badge-success">{{ $v }}</label>
            @endforeach
            @endif
        </div>
    </div>
</div>
@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/morris.min.js') }}"></script>       
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/d3.v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/rickshaw.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>                
<script type='text/javascript' src="{{ asset('joli/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
<script type="text/javascript" src="{{ asset('joli/js/plugins/owl/owl.carousel.min.js') }}"></script>                 

<script type="text/javascript" src="{{ asset('joli/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
@endsection