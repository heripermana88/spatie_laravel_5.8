@extends('layouts.joli.main')

@section('title',' Current User : user')

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-body">
               
               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">Password</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    
                                    User Information
                                    <hr>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    Change Password
                                    <hr>
                                    <div class="form-group">
                                        <label>Current Password</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>New Password</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                    <div class="form-group">
                                        <label>Confirm New Password</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                
                                </div>
                           </div>
                     </div>
               </div>
               
               <br>
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

@endsection