@extends('layouts.joli.main')

@section('title',' List Users')
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                    <input type="text" name="" id=""> <input type="button" value="Search">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="pull-right">
                            <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-actions">
                        <tr>
                            <!-- <th>No</th> -->
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($data as $key => $user)
                        <tr>
                            <!-- <td>{{ ++$i }}</td> -->
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                <label class="badge badge-success">{{ $v }}</label>
                                @endforeach
                                @endif
                            </td>
                            <td>{{ $user->status }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                <a class="btn btn-primary" href="{{ route('setting.user.edit',$user->id) }}">Edit</a>
                                {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                {!! $data->render() !!}
            </div>
        </div>
    
    </div>
</div>


@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/raphael-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/morris/morris.min.js') }}"></script>       
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/d3.v3.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/rickshaw/rickshaw.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('joli/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>                
<script type='text/javascript' src="{{ asset('joli/js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
<script type="text/javascript" src="{{ asset('joli/js/plugins/owl/owl.carousel.min.js') }}"></script>                 

<script type="text/javascript" src="{{ asset('joli/js/plugins/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('joli/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
@endsection
