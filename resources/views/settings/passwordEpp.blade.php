@extends('layouts.joli.main')

@section('title',' Password')

@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12">
        
        <div class="panel panel-default">
            <div class="panel-body">
               
               <div class="panel panel-success">
                     <div class="panel-heading">
                           <h3 class="panel-title">Password</h3>
                     </div>
                     <div class="panel-body">
                           <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label>EPP Password</label>
                                        <input type="email" class="form-control">
                                    </div>    
                                </div>
                                <div class="col-lg-6 col-md-6">
                                
                                </div>
                           </div>
                     </div>
               </div>
               
               <br>
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('js_plugin')
<script type='text/javascript' src="{{ asset('joli/js/plugins/icheck/icheck.min.js') }}"></script>        
<script type="text/javascript" src="{{ asset('joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
{{-- <script type="text/javascript" src="{{ asset('joli/js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script> --}}

@endsection