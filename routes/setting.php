<?php

Route::group(['middleware' => ['auth']], function() {
    Route::get('settings/contactDetails', function(){
        return view('settings.contactDetail');
    });

    Route::get('settings/passwordEpp', function(){
        return view('settings.passwordEpp');
    });
    
    Route::get('settings/users', 'SettingController@users')->name('settings.users');
    Route::get('settings/users/{id}', 'SettingController@users')->name('setting.user.edit');
    
    Route::get('settings/editProfile', function(){
        return view('settings.editProfile');
    });
});