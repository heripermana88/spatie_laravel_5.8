    <?php

    use Illuminate\Database\Seeder;

    class menuTablesSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $menus = [
                [
                    "name" => "Dashboard",
                    "href" => '/',
                    "icon" => 'fa-desktop',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "status"=>'active'
                ],
                [
                    "name" => "Domain & Contact",
                    "href" => 'domain_contact',
                    "icon" => 'fa-globe',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "status" => 'active',
                    "subs" => [
                        [
                            "name" => "Manage Domains",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                            "subs" => [
                                [
                                    "name" => "Child Nameserver",
                                    "href" => '',
                                    "icon" => '',
                                    "slug" => '',
                                    "parent_id" => '',
                                    "menu_id" => '',
                                    "sequence" => '',
                                    "status" => 'active',
                                ],
                                [
                                    "name" => "Manage nameserver",
                                    "href" => '',
                                    "icon" => '',
                                    "slug" => '',
                                    "parent_id" => '',
                                    "menu_id" => '',
                                    "sequence" => '',
                                    "status" => 'active',
                                ],
                                [
                                    "name" => "DNSSEC",
                                    "href" => '',
                                    "icon" => '',
                                    "slug" => '',
                                    "parent_id" => '',
                                    "menu_id" => '',
                                    "sequence" => '',
                                    "status" => 'active',
                                ],
                                [
                                    "name" => "EPP Keys",
                                    "href" => '',
                                    "icon" => '',
                                    "slug" => '',
                                    "parent_id" => '',
                                    "menu_id" => '',
                                    "sequence" => '',
                                    "status" => 'active',
                                ],
                                [
                                    "name" => "Privacy protection",
                                    "href" => '',
                                    "icon" => '',
                                    "slug" => '',
                                    "parent_id" => '',
                                    "menu_id" => '',
                                    "sequence" => '',
                                    "status" => 'active',
                                ],
                                
                            ],
                        ],
                        [
                            "name" => "Add New Domains",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Transfer Domains",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Manage Contacts",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Manage Documents",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ]
                    ]
                ],
                [
                    "name" => "Financials",
                    "href" => 'financials',
                    "icon" => 'fa-eur',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "status" => 'active',
                    "subs" => [
                        [
                            "name" => "Top Up",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Emergency Credit",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "List Transaction",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Pending Items",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Pricing",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Rebate Claim (pending)",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Rebate Request (pending)",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ]
                    ]
                ],
                [
                    "name" => "Tools & Logs",
                    "href" => 'tools_logs',
                    "icon" => 'fa-wrench',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "subs" => [
                        [
                            "name" => "Nameservers",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "WHOIS & Availability",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Event List (pending)",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "EPP Frame Validator",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "EPP Logs",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "EPP Message Queue",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ]
                    ]
                ],
                [
                    "name" => "Reports",
                    "href" => 'reports',
                    "icon" => 'fa-file-o',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "subs" => [
                        [
                            "name" => "Domains Management",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Revenue",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "EPP Command Statistics",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Domains Per Extension",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Monthly Extension Report",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Domain Drop List",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Billable Transactions",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ]
                    ]
                ],
                [
                    "name" => "Support",
                    "href" => 'support',
                    "icon" => 'fa-tags',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "subs" => [
                        [
                            "name" => "Tickets (pending)",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ],
                        [
                            "name" => "Server SSL Certificates",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => ''
                        ]
                    ]
                ],
                [
                    "name" => "Settings",
                    "href" => 'settings',
                    "icon" => 'fa-gear',
                    "slug" => '',
                    "parent_id" => '',
                    "menu_id" => '',
                    "sequence" => '',
                    "status" => 'active',
                    "subs" => [
                        [
                            "name" => "Contact Details",
                            "href" => 'settings/contactDetails',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Passwords",
                            "href" => 'settings/passwordEpp',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Users",
                            "href" => 'settings/users',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Test Accounts",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Access List",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "SSL Certificates",
                            "href" => '',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ],
                        [
                            "name" => "Edit My Profile",
                            "href" => 'settings/editProfile',
                            "icon" => '',
                            "slug" => '',
                            "parent_id" => '',
                            "menu_id" => '',
                            "sequence" => '',
                            "status" => 'active',
                        ]
                    ]
                ]
            ];
            $x = 1;
            foreach ($menus as $key => $m) {
                $menu = [
                    "name" => $m['name'],
                    "href" => $m['href'],
                    "icon" => $m['icon'],
                    "slug" => array_key_exists("subs", $m) ? 'dropdown' : 'link',
                    "parent_id" => null,
                    "menu_id" => 1,
                    "sequence" => $x
                ];
                if(array_key_exists('status',$m)){
                    $menu['status'] = $m['status'];
                }
                $p = $x;
                $x++;
                DB::table('menus')->insert($menu);
                if (array_key_exists("subs", $m)) {
                    foreach ($m['subs'] as $key => $mm) {
                        $pp = $x;
                        $x++;
                        $sub = [
                            "name" => $mm['name'],
                            "href" => $mm['href'],
                            "icon" => $mm['icon'],
                            "slug" => array_key_exists("subs", $mm)? 'dropdown':'link',
                            "parent_id" => $p,
                            "menu_id" => 1,
                            "sequence" => $x
                        ];
                        if(array_key_exists('status',$mm)){
                            $sub['status'] = $mm['status'];
                        }
                        DB::table('menus')->insert($sub);

                        if (array_key_exists("subs", $mm)) {
                            foreach ($mm['subs'] as $km => $mmm) {
                                $x++;
                                $subsub = [
                                    "name" => $mmm['name'],
                                    "href" => $mmm['href'],
                                    "icon" => $mmm['icon'],
                                    "slug" => 'link',
                                    "parent_id" => $pp,
                                    "menu_id" => 1,
                                    "sequence" => $x
                                ];
                                if(array_key_exists('status',$mm)){
                                    $subsub['status'] = $mmm['status'];
                                }
                                DB::table('menus')->insert($subsub);
                            }
                        }
                    }
                }
            }
        }
    }
