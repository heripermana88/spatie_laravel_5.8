<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'INA17', 
            'username' => 'admin', 
            'email' => 'admin@ina17.id',
            'password' => bcrypt('12345678')
        ]);

        $admin_role = Role::create(['name' => 'Admin']);
        $permissions = Permission::pluck('id','id')->all();
        $admin_role->syncPermissions($permissions);
        $admin->assignRole([$admin_role->id]);

        $registran = User::create([
            'name' => 'INA17', 
            'username' => 'registran', 
            'email' => 'registran@ina17.id',
            'password' => bcrypt('12345678')
        ]);

        $registran_role = Role::create(['name' => 'Registran']);
        $permissions = Permission::pluck('id','id')->all();
        $registran_role->syncPermissions($permissions);
        $registran->assignRole([$registran_role->id]);


        Role::create(['name' => 'Finance']);
        Role::create(['name' => 'Technical']);
    }
}
