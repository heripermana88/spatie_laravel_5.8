<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Spatie\Permission\Models\Role;

class SettingController extends Controller
{
    public function contactDetail()
    {

    }

    public function passwordEpp()
    {

    }

    public function users(Request $request,$id=null)
    {
        if($id==null){
            $data = User::orderBy('id','DESC')->paginate(5);
            return view('settings.users',compact('data'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
        }else{
            $user = User::find($id);
            $roles = Role::pluck('name','name')->all();
            $userRole = $user->roles->pluck('name','name')->all();

            return view('settings.usersEdit',compact('user','roles','userRole'));
        }
    }
}
